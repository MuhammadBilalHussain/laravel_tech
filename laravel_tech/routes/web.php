<?php

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('login',[AuthController::class,'loginView'])->name('login');
Route::get('register',[AuthController::class,'registerView'])->name('register');
Route::post('save-user',[AuthController::class,'saveUser'])->name('saveUser');
Route::post('make-login',[AuthController::class,'login'])->name('makeLogin');
Route::get('forgot-password',[AuthController::class,'forgotPasswordView'])->name('forgotPasswordView');
Route::post('do-forgot-password',[AuthController::class,'forgotPassword'])->name('forgotPassword');
Route::get('reset-password',[AuthController::class,'resetPassword'])->name('resetPassword');
Route::post('change-password',[AuthController::class,'changePassword'])->name('changePassword');

Route::group(['middleware'=>['auth']],function (){
    Route::get('dashboard',function (){
        return view('admin.index');
    });

    Route::get('logout',[AuthController::class,'logout'])->name('logout');
});
