@extends('layout.unauth_layout')
@section('content')





    <div class="wrapper w-50 mt-5">
        <div class="card">
            <div class="card-header">
                Forgot Password
            </div>
            <div class="card-body">
                <form action="{{route('forgotPassword')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" placeholder="Enter Email" name="email">
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12 d-flex justify-content-between">
                            <button class="btn btn-success mt-2">Sent Verification Mail</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>






@endsection
