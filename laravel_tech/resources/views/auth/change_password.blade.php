@extends('layout.unauth_layout')
@section('content')
    <div class="wrapper w-50 mt-5">
        <div class="card">
            <div class="card-header">
                Change Password
            </div>
            <div class="card-body">
                <form action="{{route('changePassword')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" placeholder="Enter Password" name="password">
{{--                            @error('password')--}}
{{--                                <span class="text-danger">{{$errors->first('password')}}</span>--}}
{{--                            @enderror--}}
                        </div>
                        <div class="col-md-6">
                            <label for="password_confirmation">Confirm password</label>
                            <input type="password" class="form-control" placeholder="Enter Password" name="password_confirmation">
                        </div>
                        <input type="hidden" value="{{$email}}" name="email">
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12 d-flex justify-content-between">
                            <button class="btn btn-success mt-2">change</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
