@extends('layout.unauth_layout')
@section('content')

    <div class="wrapper w-50 mt-5">
        <div class="card">
            <div class="card-header">
                Register
            </div>
            <div class="card-body">
                <form action="{{route('saveUser')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" placeholder="Enter Name" name="name">
                            @error('name')
                            <span class="text-danger">{{$errors->first('name')}}</span>
                            @enderror
                        </div>
                        <div class="col-md-12">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" placeholder="Enter Email" name="email">
                            @error('email')
                            <span class="text-danger">{{$errors->first('email')}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 mt-2">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" placeholder="Enter Password" name="password" id="password">
                            @error('password')
                            <span class="text-danger">{{$errors->first('password')}}</span>
                            @enderror
                        </div>
                        <div class="col-md-4 mt-4">
{{--                            <label for="">&nbsp;</label>--}}
                            <button onclick="generatePassword()" type="button">Generate password</button>
                        </div>
                        <div class="col-md-12 mt-2">
                            <label for="password">Confirm Password</label>
                            <input type="password" class="form-control" placeholder="Enter Password" name="password_confirmation" id="confirm_password">
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <button class="btn btn-success mt-2">Register</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script>
        function generatePassword() {
            let randomPassword = randomString();
            console.log(randomPassword);
            document.getElementById('password').value = randomPassword; //set value
            document.getElementById('confirm_password').value = randomPassword; //set value
        }

        function randomString() {
            //initialize a variable having alpha-numeric characters
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

            //specify the length for the new string to be generated
            var string_length = 8;
            var randomstring = '';

            //put a loop to select a character randomly in each iteration
            for (var i=0; i<string_length; i++) {
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum,rnum+1);
            }
            return randomstring;
        }
        // generatePassword();
    </script>
@endpush
