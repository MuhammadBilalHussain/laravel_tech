@extends('layout.unauth_layout')
@section('content')

   <div class="wrapper w-50 mt-5">
       <div class="card">
           <div class="card-header">
               login
           </div>
           <div class="card-body">
               <form action="{{route('makeLogin')}}" method="post">
                   @csrf
                   <div class="row">
                       <div class="col-md-12">
                           <label for="email">Email</label>
                           <input type="email" class="form-control" placeholder="Enter Email" name="email">
                       </div>
                       <div class="col-md-12 mt-2">
                           <label for="password">Password</label>
                           <input type="password" class="form-control" placeholder="Enter Password" name="password">
                       </div>
                   </div>
                   <div class="row mt-2">
                       <div class="col-md-12 d-flex justify-content-between">
                           <button class="btn btn-success mt-2">Login</button>
                           <a href="{{route('forgotPasswordView')}}" class="btn btn-primary mt-2">Forgot Password</a>
                       </div>
                   </div>
               </form>
           </div>
       </div>
   </div>

@endsection
