<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forgot Password Mail</title>
</head>
<body>
    <h2>Testing EMail</h2>

    <a href="{{url('reset-password').'?email='.$data['email'].'&token='.$data['token']}}">Click here to verify your email</a>
</body>
</html>
