<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ForgotPasswordMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function loginView(){
        return view('auth.login');
    }
    public function registerView(){
        return view('auth.register');
    }


    public function saveUser(Request $request){
        $this->validate($request,[
           'name'=>'required',
           'email'=>'required|email|unique:users,email',
           'password'=>'required|confirmed'
        ]);

        $user = User::create([
           'name'=>$request->name,
           'email'=>$request->email,
           'password'=>Hash::make($request->password),
        ]);

        Auth::loginUsingId($user->id);
        return redirect('dashboard');
    }

    public function login(Request $request){
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required'
        ]);

        $arr = [
          'email' => $request->email,
          'password' => $request->password
        ];

        if(Auth::attempt($arr)){
            return redirect('dashboard')->with(['success'=>'welcome']);
        }else{
            return back()->with(['error'=>'Invalid Credentials']);
        }
    }
    public function logout(){
        Auth::logout();

        return redirect('login')->with(['success'=>'Logout successfully']);
    }
    public function forgotPasswordView(){
        return view('auth.forgot_password');
    }
    public function forgotPassword(Request $request){
        $token = Str::random(10);
        $user = User::where('email',$request->email)->first();
        if ($user) {
            $data = [
                'email' => $request->email,
                'token' => $token,
                'created_at'=>now()
            ];
            DB::table('password_resets')->insert($data);
            Mail::to('your_email@gmail.com')->send(new ForgotPasswordMail($data));

            dd("Mail sent");
        } else {
            return back()->with(['error'=>'Email not exist']);
        }
    }

    public function resetPassword(){
        $email = \request()->email;
        $token = \request()->token;
        $data = DB::table('password_resets')->where('email',$email)
            ->where('token',$token)->latest()->first();

        if ($data){
            return view('auth.change_password',compact('email'));
        } else {
            return redirect('login')->with(['error'=>'Token not match']);

        }
    }
    public function changePassword(Request $request){
        $this->validate($request,[
            'password'=>'required|confirmed'
        ]);

        User::where('email',$request->email)->update([
           'password'=>Hash::make($request->password)
        ]);
        return redirect('login')->with(['success'=>'Password changed Successfully']);
    }
}
